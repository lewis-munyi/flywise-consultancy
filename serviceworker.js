var CACHE_NAME = "flywise-cache",
    resources = ["/pwa/manifest.json", "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js", "js/styles.js", "/css/navigation.css", "js/scripts.js", "js/jquery.themepunch.tools.min.js", "js/jquery.themepunch.revolution.min.js", "js//wow.min.js", "images/logo5.png", "images/plane.mp4"];
self.addEventListener("install", function (e) {
    e.waitUntil(caches.open(CACHE_NAME).then(function (e) {
        return e.addAll(resources)
    }))
}), self.addEventListener("fetch", function (e) {
    e.respondWith(caches.match(e.request).then(function (s) {
        return s ? s : fetch(e.request)
    }))
});