/* Bootstrap */ 
import './css/bootstrap.css';

/* Fonts */
import './css/simple-line-icons.css';
import './css/font-awesome.css';

/* Animations */
import './vendors/animate/css/animate.min.css';

/*  revolution-slider  */
import './vendors/revolution-slider/rs-plugin/css/settings.css';
import './vendors/revolution-slider/css/layers.css';

/* Carousel */
import './vendors/OwlCarousel/owl-carousel/owl.carousel.css';
import './vendors/OwlCarousel/owl-carousel/owl.theme.css';

/* Custom styles */
import './css/custom.css';