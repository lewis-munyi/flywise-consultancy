module.exports = {
    entry: {
        styles: ['./styles.js'], //Load all styles
        scripts: ['./scripts.js'], //Load all scripts
        contactus: ['./js/contact_us.js'],
        serviceworker: ['./serviceworker.js']
    },
    output: {
        path: __dirname + "/dist/js",
        filename: '[name].js'
    },
    watch: true,
    mode: "production",
    module: {
        rules: [{
                test: /\.css$/,
                use: ['style-loader', 'css-loader', 'sass-loader', // CSS loaders
                    {
                        loader: 'postcss-loader', // Run post css actions
                        options: {
                            plugins: function () { // post css plugins, can be exported to postcss.config.js
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                ];
                            }
                        }
                    }
                ]
            }, {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/, // Exempt node_modules and bower_components
                use: {
                    loader: 'babel-loader', // Pre-process ES6 and node to js
                    options: {
                        presets: ['env']
                    }
                }
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg|png|gif)(\?v=\d+\.\d+\.\d+)?$/,
                use: [{
                    loader: 'file-loader', //Preprocessor for images and fonts
                    options: {
                        name: '[name].[ext]',
                        outputPath: 'fonts/'
                    }
                }]
            }
        ]
    }
};