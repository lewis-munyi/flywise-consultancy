// Bootstrap
import 'bootstrap';

// Sliders and carousel
import './vendors/bxslider/js/jquery.bxslider.min.js';
import './vendors/OwlCarousel/owl-carousel/owl.carousel.js';

// Custom scripts
import './js/custom.js';
import './js/index.js';